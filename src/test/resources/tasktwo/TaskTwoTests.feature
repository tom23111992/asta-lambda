Feature: Task Two

  Background:
    Given I go to main page
    Then Should be displayed logo
    And I go to "2" task

  Scenario Outline: Check if sorting works correct
    When I sort products by category "<Category>" from dropdown list
    Then Should be displayed items with "<Category>" category only

    Examples:
      | Category       |
      | Sport          |
      | Elektronika |
      | Firma i usługi |

  Scenario Outline: Typing on search field category and checking sorting
    When I type on search field part of word "<Word>" and choose first position from list
    Then Should be products with the category

    Examples:
      | Word  |
      | Sp111 |
      | Ele   |
      | Fxxx  |

  Scenario: Searching non exists category
    When I write non exists category
    Then Should be displayed empty list