package Steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import page.MainPage;
import until.SeleniumExecutor;

import static until.SeleniumExecutor.driver;

public class CommonSteps extends AbstractTest {

    public CommonSteps() {
        this.executor = SeleniumExecutor.getExecutor();
        mainPage = new MainPage(executor);

    }

    @Given("^I go to main page$")
    public void iGoToMainPage() {
        mainPage.openPage();

    }

    @Then("^Should be displayed logo$")
    public void shouldBeDisplayedLogo() throws Throwable {
        Assert.assertTrue("Logo is not displayed", mainPage.isLogoDisplayed());
    }

    @And("^I go to \"([^\"]*)\" task$")
    public void iGoToTask(String task) throws Throwable {
        mainPage.clickTaskButton(task);
    }

    @After
    public void exitMethod(Scenario scenario) {
        if (scenario.isFailed()) {
            try {

                byte[] myScreenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(myScreenshot, "image/png");

            } catch (WebDriverException wde) {
                System.err.println(wde.getMessage());
            } catch (ClassCastException cce) {
                cce.printStackTrace();
            }
        }
        SeleniumExecutor.stop();
    }
}
