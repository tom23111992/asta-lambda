package Steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import helpers.RandomStringGenerator;
import helpers.enums.*;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.Suite;
import page.TaskTwoPage;
import until.SeleniumExecutor;

public class TwoTaskSteps extends AbstractTest {

    protected String selectedFilterValue;

    public TwoTaskSteps() {
        this.executor = SeleniumExecutor.getExecutor();
        taskTwoPage = new TaskTwoPage(executor);
    }

    @When("^I sort products by category \"([^\"]*)\" from dropdown list$")
    public void iSortProductsByCategoryFromDropdownList(String category) throws Throwable {
        taskTwoPage.selectSortCategory(category);
    }

    @When("^I type on search field part of word \"([^\"]*)\" and choose first position from list$")
    public void writeWord(String text) throws Throwable {
        selectedFilterValue = taskTwoPage.fillSearchFieldCategory(text);
    }

    @When("^I write non exists category")
    public void writeNonExistsCategory() throws Throwable {
        taskTwoPage.fillSearchField(RandomStringGenerator.randomString(10));
    }

    @Then("^Should be displayed items with \"([^\"]*)\" category only$")
    public void shouldBeDisplayedItemsWithCategoryOnly(String category) throws Throwable {
        Assert.assertTrue("Sorting works incorrectly", taskTwoPage.checkResultsByCategoryName(category));
    }

    @Then("^Should be products with the category$")
    public void checkingNames() throws Throwable {
        Assert.assertTrue("Sorting works incorrectly", taskTwoPage.checkResultsByCategoryName(selectedFilterValue));
    }

    @Then("^Should be displayed empty list$")
    public void displayInformation() throws Throwable {
        Assert.assertEquals(Messages.noFound.toString(), taskTwoPage.getSortingValue(0));
    }
}
