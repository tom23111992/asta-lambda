package Steps;

import page.MainPage;
import page.TaskTwoPage;
import until.SeleniumExecutor;

public abstract class AbstractTest {

    protected SeleniumExecutor executor;
    protected TaskTwoPage taskTwoPage;
    protected MainPage mainPage;

}
