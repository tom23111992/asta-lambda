package page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import until.SeleniumExecutor;

import java.util.List;

public class MainPage extends AbstractPage {

    @FindBy(how = How.XPATH, using = "//img[@class='main-logo']")
    private WebElement logo;

    @FindBy(how = How.XPATH, using = "//div[@class='col-md-6']/a")
    private List<WebElement> taskButtonList;

    public MainPage(SeleniumExecutor executor) {
        super(executor);
    }

    public void clickTaskButton(String taskNumber) {

        for (int i = 0; i < taskButtonList.size(); i++) {
            if (taskButtonList.get(i).getAttribute("href").equals("https://testingcup.pgs-soft.com/task_" + taskNumber)) {
                wrappedClick(taskButtonList.get(i));
                break;
            }
        }
    }

    public boolean isLogoDisplayed() {
        return elementIsPresent(logo);
    }


}
