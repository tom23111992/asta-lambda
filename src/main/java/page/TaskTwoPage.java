package page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import until.SeleniumExecutor;

import java.util.List;

public class TaskTwoPage extends AbstractPage {

    @FindBy(how = How.XPATH, using = "//span[@class='select2-selection__rendered']")
    private WebElement dropDownList;

    @FindBy(how = How.XPATH, using = "//span[@class='select2-results']/ul/li")
    private List<WebElement> fieldsSortByList;

    @FindBy(how = How.XPATH, using = "//div[@class='caption']/p[3]")
    private List<WebElement> productsNameList;

    @FindBy(how = How.XPATH, using = "//span[@class='select2-search select2-search--dropdown']/input")
    private WebElement searchField;

    private String selectedValue;

    public TaskTwoPage(SeleniumExecutor executor) {
        super(executor);
    }

    public void selectSortCategory(String category) {
        wrappedClick(dropDownList);
        clickElementFromTheList(fieldsSortByList, category);
    }

    public boolean checkResultsByCategoryName(String category) {
        boolean value = true;
        for (int i = 0; i < productsNameList.size(); i++) {
            if (!wrappedGetText(productsNameList.get(i)).equals(category)) {
                value = false;
                break;
            }
        }
        return value;
    }

    public void fillSearchField(String value) {
        wrappedClick(dropDownList);
        wrappedSendKeys(searchField, value);
    }

    public String fillSearchFieldCategory(String partWord) {
        fillSearchField(partWord);
        selectedValue = wrappedGetText(fieldsSortByList.get(0));
        wrappedClick(fieldsSortByList.get(0));
        return selectedValue;
    }

    public String getSortingValue(int i) {
        return wrappedGetText(fieldsSortByList.get(i));
    }
}
