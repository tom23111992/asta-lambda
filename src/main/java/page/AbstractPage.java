package page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import until.Configurations;
import until.SeleniumExecutor;

import java.util.List;


public class AbstractPage {

    public SeleniumExecutor executor;
    Actions actions;
    private String url;
    private Configurations configurations;

    public AbstractPage(SeleniumExecutor executor) {
        this.executor = executor;
        this.url = SeleniumExecutor.getUrl();
        //   actions = new Actions(driver);
        PageFactory.initElements(new AjaxElementLocatorFactory(executor.getDriver(), 15), this);
    }

    public void wrappedClick(WebElement element) {
        waitUntil(ExpectedConditions.visibilityOf(element));
        waitUntil(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void wrappedSendKeys(WebElement element, String keys) {
        waitUntil(ExpectedConditions.visibilityOf(element));
        waitUntil(ExpectedConditions.elementToBeClickable(element));
        element.clear();
        element.sendKeys(keys);
    }

    public String wrappedGetText(WebElement element) {
        waitUntil(ExpectedConditions.visibilityOf(element));
        waitUntil(ExpectedConditions.elementToBeClickable(element));
        return element.getText();
    }


    public void scrollToElementToDown(Object locator) {

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) executor.getDriver();
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", locator);
    }

    public void clickElementFromTheList(List<WebElement> webElementList, String element) {

        waitUntil(ExpectedConditions.visibilityOf(webElementList.get(0)));
        for (int i = 0; i < webElementList.size(); i++) {
            if (webElementList.get(i).getText().equals(element)) {
                wrappedClick(webElementList.get(i));
                break;

            } else if (!(webElementList.get(i).getText().equals(element))) {
                scrollToElementToDown(webElementList.get(i));
            }
        }
    }

    public boolean elementIsPresent(WebElement webElement) {
        try {
            return webElement.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public <A> void waitUntil(ExpectedCondition<A> conditions) {
        WebDriverWait w8 = new WebDriverWait(executor.getDriver(), 15);
        w8.until(conditions);
    }

    public void openPage() {
        executor.openPage(Configurations.defaultUrl);
    }

}
